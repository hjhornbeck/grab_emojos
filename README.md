# grab_emojos

A quick-and-dirty Python script to download all the "[emojos](https://emojos.in/)" from a Mastodon server. Dumps them in a directory of your choosing, after which you can run

```bash
tar -czf ../emojos.tar.gz *
```

to convert them into format ready for a [`tootctl emoji import`](https://docs.joinmastodon.org/admin/tootctl/#emoji-import). No category information is saved,
but since few servers currently return that info there's no point.

## Options

```bash
$ python3 grab_emojos.py --help
usage: grab_emojos.py [-h] [-o DIRECTORY] [-r] HOST.NAME

Download and rename the emojos from a Mastodon server.

positional arguments:
  HOST.NAME             The Mastodon host to download from.

options:
  -h, --help            show this help message and exit
  -o DIRECTORY, --outdir DIRECTORY
                        The location to store all emojos.
  -r, --replace         Should we replace existing emojos, or let them be?
```
