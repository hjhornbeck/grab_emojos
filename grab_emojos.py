#!/usr/bin/env python3

##### IMPORTS

import argparse
import json
from os.path import basename, exists
import re
from urllib.request import urlopen, Request


##### GLOBALS

headers = {'user-agent': 'curl/7.81.0', 'accept': '*/*'}

hostname = re.compile(r'^https?://([^/]+)')

##### FUNCTIONS

# based on Allen Downey's code from Think Bayes 2

def download(url, filename=None, replace=False, headers=dict()):

	result = hostname.match( url )
	if result is None:
		return False
	headers['Host'] = result.groups()[0]

	if filename is None:
		filename = basename( url )
	if replace or (not exists(filename)):
		req = Request( url, headers=headers )
		with urlopen( req ) as input:
			with open( filename, 'wb' ) as output:
				output.write( input.read() ) 
		return True
	return False

##### MAIN

parser = argparse.ArgumentParser( description='Download and rename the emojos from a Mastodon server.' )
parser.add_argument('hostname', metavar='HOST.NAME', help="The Mastodon host to download from." )
parser.add_argument('-o','--outdir', metavar="DIRECTORY", default='.', help="The location to store all emojos." )
parser.add_argument('-r','--replace', action='store_true', help="Should we replace existing emojos, or let them be?" )
args = parser.parse_args()

headers['Host'] = args.hostname


req = Request( f'https://{args.hostname}/api/v1/custom_emojis', headers=headers )
with urlopen( req ) as f:
	response = json.load( f )

for item in response:

	if 'shortcode' not in item:
		print('ERROR: no shortcode present!')
		continue
	if 'static_url' not in item:
		print('ERROR: no static_url present!')
		continue

	if item['static_url'][-4:] in {'.png', '.PNG'}:	
		filename = '.png'
	elif item['static_url'][-4:] in {'.gif', '.GIF'}:	
		filename = '.gif'
	else:
		print(f'ERROR: Could not detect file type of {item["static_url"]}, please update the code!')

	filename = item['shortcode'] + filename

	try:
		if download( item['static_url'], f'{args.outdir}/{filename}', args.replace, headers ):
			print( f'Downloaded {filename}' )
		continue
	except:
		pass

	# if we got here, something went wrong
	if 'url' not in item:
		continue
	if download( item['url'], f'{args.outdir}/{filename}', args.replace, headers ):
		print( f'Downloaded {filename}' )


